﻿namespace MainMicroService.Models
{
    public class BasicRegisterResult
    {
        #region Properties

        public string Email { get; set; }

        public string Code { get; set; }

        #endregion
    }
}